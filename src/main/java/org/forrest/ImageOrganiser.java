package org.forrest;

import static com.google.common.io.Files.getFileExtension;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.groupingByConcurrent;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.io.FileUtils.contentEquals;
import static org.forrest.ImgDiffPercent.diff;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.forrest.Reports.DuplicateImageReportEntry;
import org.forrest.Reports.ErrorReportEntry;
import org.forrest.Reports.MovedReportEntry;
import org.forrest.Reports.ReportEntry;
import org.forrest.Reports.ReportVisitor;
import org.forrest.Reports.SameImageReportEntry;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Either;

public final class ImageOrganiser {

    private static final Logger logger = Logger.getLogger(ImageOrganiser.class);

    private static final double ACCEPTABLE_DIFF = 3.5d;

    private static final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .appendLiteral("img")
            .appendPattern("yyyyMMddHHmmss")
            .toFormatter();
    private static final ReportVisitor reportVisitor = new LogReportVisitor();

    public static void main(String... args) throws IOException, ParseException {
        CommandLine line = parseOptions(args);
        Path source = Paths.get(line.getOptionValue('s'));
        Path destination = Paths.get(line.getOptionValue('d'));

        if (Files.notExists(source)) {
            throw new IllegalArgumentException("Source path does not exist: " + source);
        }

        if (Files.notExists(destination)) {
            throw new IllegalArgumentException("Destination path does not exist: " + destination);
        }

        new ImageOrganiser().start(source, destination);
    }

    private static CommandLine parseOptions(String... args) throws ParseException {
        Options options = new Options();
        options.addOption(Option.builder("s")
                .longOpt("source")
                .type(String.class)
                .required()
                .argName("path")
                .hasArg()
                .desc("Source location for photos to restructure")
                .build());
        options.addOption(Option.builder("d")
                .longOpt("destination")
                .required()
                .type(String.class)
                .argName("path")
                .hasArg()
                .desc("Destination location for photos to restructure")
                .build());

        try {
            CommandLineParser parser = new DefaultParser();
            return parser.parse(options, args);
        } catch (ParseException pE) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Image Organiser", options);
            throw pE;
        }
    }

    private void start(Path source, Path destination) throws IOException {
        List<Path> files = getFileWithin(source);

        if (logger.isDebugEnabled()) {
            logger.debug("Found " + files.size() + " files under " + source);
        }

        Tuple2<List<Tuple2<LocalDateTime, Path>>, List<ReportEntry>> parsedImagesWithReport = files.stream()
                .parallel()
                .map(this::extractCreationDate)
                .map(either -> either.fold(left -> Tuple.of(singletonList(left), Collections.<ReportEntry>emptyList()),
                        right -> Tuple.of(Collections.<Tuple2<LocalDateTime, Path>>emptyList(), singletonList(right))))
                .reduce((t1, t2) -> Tuple.of(merge(t1._1, t2._1), merge(t1._2, t2._2)))
                .orElseThrow(() -> new IllegalArgumentException("Unable to process images in " + source));

        Map<LocalDateTime, List<Path>> imagesIndexedByCreationDate = parsedImagesWithReport._1.stream()
                .parallel()
                .collect(groupingByConcurrent(Tuple2::_1, mapping(Tuple2::_2, toList())));

        Stream<ReportEntry> reports = imagesIndexedByCreationDate.entrySet().stream()
                .parallel()
                .map(imageBatch -> restructureImages(imageBatch, destination))
                .flatMap(Collection::stream);

        Map<Class<? extends ReportEntry>, List<ReportEntry>> groupedReports =
                Stream.concat(parsedImagesWithReport._2.stream(), reports)
                        .parallel()
                        .collect(groupingByConcurrent(ReportEntry::getType, mapping(Function.identity(), toList())));

        logReports(groupedReports);
    }

    private List<Path> getFileWithin(Path source) throws IOException {
        logger.debug("Reading files from source " + source);
        try (Stream<Path> stream = Files.walk(source)) {
            return stream.filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        }
    }

    private Either<Tuple2<LocalDateTime, Path>, ReportEntry> extractCreationDate(Path file) {
        logger.debug("Attempting to extract creation date from exif data in " + file);
        try (InputStream stream = Files.newInputStream(file)) {
            Metadata metadata = ImageMetadataReader.readMetadata(stream);

            return Optional.ofNullable(metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class))
                    .map(ExifSubIFDDirectory::getDateOriginal)
                    .map(creationDate -> creationDate.toInstant().atZone(ZoneId.systemDefault()))
                    .map(ZonedDateTime::toLocalDateTime)
                    .map(creationDateTime -> Either
                            .<Tuple2<LocalDateTime, Path>, ReportEntry>left(Tuple.of(creationDateTime, file)))
                    .orElseGet(() -> Either.right(Reports.error("Unable to extract creation date from " + file)));

        } catch (IOException | ImageProcessingException e) {
            return Either.right(Reports.error("Unable to process file " + file, e));
        }
    }

    private List<ReportEntry> restructureImages(Entry<LocalDateTime, List<Path>> imageBatch, Path destination) {
        if (logger.isDebugEnabled()) {
            logger.debug("Processing image batch (" + imageBatch.getValue().size()
                    + ") grouped by creation date " + imageBatch.getKey());
        }

        return imageBatch.getValue()
                .stream()
                .map(image -> moveImage(imageBatch.getKey(), image, destination))
                .collect(Collectors.toList());
    }

    private ReportEntry moveImage(LocalDateTime creationDate, Path image, Path destination) {
        String year = String.valueOf(creationDate.getYear());
        String month = String.valueOf(creationDate.getMonthValue());
        Path actualDestination = destination.resolve(year).resolve(month);
        String prefix = formatter.format(creationDate);
        return resolveNewImageName(image, actualDestination, prefix, 0)
                .fold(newImagePath -> moveImage(image, newImagePath, actualDestination), Function.identity());
    }

    private Either<Path, ReportEntry> resolveNewImageName(Path image, Path destination, String prefix, int count) {
        StringBuilder newImageName = new StringBuilder(prefix);

        if (count > 0) {
            newImageName.append('(')
                    .append(count)
                    .append(')');
        }

        newImageName.append('.').append(getFileExtension(image.toString()));
        Path newImagePath = destination.resolve(newImageName.toString());

        if (Files.notExists(newImagePath)) {
            return Either.left(newImagePath);
        }

        try {
            if (image.equals(newImagePath)) {
                return Either.right(Reports.same(image));
            }

            File imageFile = image.toFile();
            File existingFile = newImagePath.toFile();

            if (contentEquals(imageFile, existingFile)) {
                return Either.right(Reports.duplicate(image, newImagePath));
            }

            double diff = diff(imageFile, existingFile);

            if (diff < ACCEPTABLE_DIFF) {
                return Either.right(Reports.duplicate(image, newImagePath, diff));
            }
        } catch (IOException e) {
            return Either.right(Reports.error("Unable to compare images " + image + " and " + newImagePath, e));
        }

        return resolveNewImageName(image, destination, prefix, ++count);
    }

    private ReportEntry moveImage(Path image, Path newImagePath, Path destination) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Moving image " + image + " to " + newImagePath);
            }

            Files.createDirectories(destination);
            Files.move(image, newImagePath);
            return Reports.move(image, newImagePath);
        } catch (IOException e) {
            return Reports.error("Failed to move image " + image + " to " + newImagePath, e);
        }
    }

    private void logReports(Map<Class<? extends ReportEntry>, List<ReportEntry>> groupedReports) {
        if (logger.isDebugEnabled()) {
            logReportGroup(groupedReports.get(SameImageReportEntry.class),
                    count -> logger.debug("Images already organised (" + count + "):"));
            logReportGroup(groupedReports.get(MovedReportEntry.class),
                    count -> logger.debug("Moved images (" + count + "):"));
        }

        logReportGroup(groupedReports.get(DuplicateImageReportEntry.class),
                count -> logger.warn("Duplicate images (" + count + "):"));
        logReportGroup(groupedReports.get(ErrorReportEntry.class),
                count -> logger.error("Errors (" + count + "):"));
    }

    private void logReportGroup(List<ReportEntry> reportGroup, Consumer<Integer> reportCount) {
        Optional.ofNullable(reportGroup)
                .ifPresent(reports -> {
                    reportCount.accept(reports.size());
                    reports.forEach(r -> r.accept(reportVisitor));
                });
    }

    private <T> List<T> merge(List<T> list1, List<T> list2) {
        return Stream.concat(list1.stream(), list2.stream())
                .collect(Collectors.toList());
    }

    private static final class LogReportVisitor implements ReportVisitor {

        @Override
        public void visit(SameImageReportEntry entry) {
            logger.debug("Already organised: " + entry.image);
        }

        @Override
        public void visit(DuplicateImageReportEntry entry) {
            logger.warn("Duplicate: " + entry.image + ", existing: "
                    + entry.existingImage + " (diff: " + entry.diff + "%)");
        }

        @Override
        public void visit(MovedReportEntry entry) {
            logger.debug("Moved from: " + entry.from + ", to: " + entry.to);
        }

        @Override
        public void visit(ErrorReportEntry entry) {
            logger.error("Error: " + entry.errorMessage, entry.exception);
        }

    }

}
