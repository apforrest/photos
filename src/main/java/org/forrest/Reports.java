package org.forrest;

import java.nio.file.Path;

final class Reports {

    private static final double PERFECT_MATCH = 0.0d;

    private Reports() {
        throw new IllegalAccessError();
    }

    static ReportEntry error(String message, Exception e) {
        return new ErrorReportEntry(message, e);
    }

    static ReportEntry error(String message) {
        return error(message, null);
    }

    static ReportEntry move(Path from, Path to) {
        return new MovedReportEntry(from, to);
    }

    static ReportEntry same(Path image) {
        return new SameImageReportEntry(image);
    }

    static ReportEntry duplicate(Path duplicateImage, Path existingImage) {
        return duplicate(duplicateImage, existingImage, PERFECT_MATCH);
    }

    static ReportEntry duplicate(Path duplicateImage, Path existingImage, double diff) {
        return new DuplicateImageReportEntry(duplicateImage, existingImage, diff);
    }

    static class ErrorReportEntry implements ReportEntry {

        final String errorMessage;
        final Exception exception;

        private ErrorReportEntry(String errorMessage, Exception exception) {
            this.errorMessage = errorMessage;
            this.exception = exception;
        }

        @Override
        public void accept(ReportVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public Class<? extends ReportEntry> getType() {
            return ErrorReportEntry.class;
        }

    }

    static class DuplicateImageReportEntry implements ReportEntry {

        final Path image;
        final Path existingImage;
        final double diff;

        private DuplicateImageReportEntry(Path image, Path existingImage, double diff) {
            this.image = image;
            this.existingImage = existingImage;
            this.diff = diff;
        }

        @Override
        public void accept(ReportVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public Class<? extends ReportEntry> getType() {
            return DuplicateImageReportEntry.class;
        }

    }

    static class SameImageReportEntry implements ReportEntry {

        final Path image;

        private SameImageReportEntry(Path image) {
            this.image = image;
        }

        @Override
        public void accept(ReportVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public Class<? extends ReportEntry> getType() {
            return SameImageReportEntry.class;
        }

    }

    static class MovedReportEntry implements ReportEntry {

        final Path from;
        final Path to;

        private MovedReportEntry(Path from, Path to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public void accept(ReportVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public Class<? extends ReportEntry> getType() {
            return MovedReportEntry.class;
        }

    }

    interface ReportEntry {

        void accept(ReportVisitor visitor);

        Class<? extends ReportEntry> getType();

    }

    interface ReportVisitor {

        void visit(SameImageReportEntry entry);

        void visit(DuplicateImageReportEntry entry);

        void visit(MovedReportEntry entry);

        void visit(ErrorReportEntry entry);

    }

}
